$(function() {

    function controlTicket() {
        var options = $('.options');
        var types = $('.types li');

        options.find('div').addClass('hidden');

        types.on('click', function(e) {
            e.preventDefault();
            var dataTarget = $(this).data('target');
            
            $(types).removeClass('active');
            $(this).addClass('active');
            options.find('div').addClass('hidden');
            options.find(`.${dataTarget}`).removeClass('hidden');
        });
    }

    controlTicket();

});