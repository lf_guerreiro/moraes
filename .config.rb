# preferred_syntax = :scss


http_path = '/'
css_dir = './.tmp/app/'
sass_dir = './app/styles/'
images_dir = './app/images'
images_path = './app/images/'
http_images_path = ''
generated_images_path = 'images/'
##javascripts_dir = 'assets/javascripts'
relative_assets = false
line_comments = true
#output_style = :compressed